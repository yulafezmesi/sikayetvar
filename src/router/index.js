import { createWebHistory, createRouter } from "vue-router";
import Post from "../components/Post/Index.vue";
import Profile from "../components/ProfileDetail.vue";
import PostDetail from "../components/Post/PostDetail.vue";

const routes = [
  {
    path: "/",
    name: "Post",
    component: Post,
  },
  {
    path: "/profile-detail/:id",
    name: "ProfileDetailId",
    component: Profile,
  },
  {
    path: "/post-detail/:id",
    name: "PostDetail",
    component: PostDetail,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
